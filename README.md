XSLT transformations for conversion of HTML Typescript (as emitted by XSweet) into an "Editoria-flavored" HTML for import by Editoria.

For comprehensive Editoria Typescript documentation, visit https://xsweet.org/editoria-typescript/.

See project roadmap information at https://gitlab.coko.foundation/XSweet/XSweet.

Check out the other XSweet tools at https://gitlab.coko.foundation/XSweet.
